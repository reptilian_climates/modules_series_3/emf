


'''
constants: [{
	ellipse: 1,
	
	escrow: {
		#
		#	After 10000 aggregates the escrowed funds are returned.
		#
		aggregate_limit: 10000,
		
		petitions: {
			#
			#	>50% of the vote is required for a petition to be commitable
			#
			votes_required: [ 1, 2 ]
		},
	},
	
	aggregates: {
		step_limit: 1000
	}
}]
'''

