





def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])




import EMF.modules.ed448.private.creator as ed448_private_key_creator
import EMF.modules.ed448.public.creator as ed448_public_key_creator

seed = "FF86888b11358bf3d541b41eea5daece1c6eff64130a45fc8b9ca48f3e0e02463c99c5aedc8a847686d669b7d547c18fe448fc5111ca88f4e8"

import pathlib
from os.path import dirname, join, normpath
import os

# formats = [ "DER", "PEM" ]
format = "PEM"
format = "DER"

private_key_path = normpath (join (pathlib.Path (__file__).parent.resolve (), "ed448_private_key")) + "." + format
public_key_path = normpath (join (pathlib.Path (__file__).parent.resolve (), "ed448_public_key")) + "." + format

private_key = ed448_private_key_creator.create (
	seed, 
	format, 
	private_key_path
)


FP = open("ed448_private_key.DER", "rb")
print (FP.read ()) 